class Util {
  zip(a, b) {
    return a.map((k, i) => [k, b[i]]);
  }

  asArray(x) {
    return Array.isArray(x) ? x : Array(x)
  }
  
  sum(arr) {
    return arr.reduce(((a,v) => a + v),0)
  }
  
  isIntArray(arg) {
    return Array.isArray(arg) && Number.isInteger(arg[0])
  }
  
  isValidTuple(tuple) {
    return Array.isArray(tuple)
    && tuple.length == 3
    && typeof tuple[0] === 'string'
    && typeof tuple[1] === 'string'
    && typeof tuple[2] === 'number'
  }
  
  isSingleTuple(arg) {
    return this.isValidTuple(arg)
  }
  
  isCompoundList(arg) {
    return arg.filter(elem => this.isValidTuple(elem) 
      || Number.isInteger(elem))
  }
  
  isListOfTuples(arg) {
    return arg.filter(isValidTuple).length == arg.length
  }
  
  extractNoteData(query) {
    return query.value.values.map(x => x[0])
  }
  
  ToHoursAndMinutes(totalMinutes) {
     let hours = Math.floor(totalMinutes / 60);
     let minutes = totalMinutes % 60;
     return `${hours} hours and ${minutes} minutes`
  }
}


