class Piano {
  getSightReadingTime(dv) {
    const note = dv.page("Piano/Practice/Sight-Reading.md")
    return this.sumPlayingTime(note);
  }

  async getDailyEntryCount(dv) {
    // TODO rework to use dv.pages("Piano/Daily"), test performance difference compared to query
    const dailyNoteQuery = await dv.query(`
      TABLE
      FROM "Piano/Daily" 
    `)

    return dailyNoteQuery.value.values.length;
  }

  async extractNoteData(dv) {
    const {Util} = customJS

    // TODO rework to use dv.pages("Piano/Practice") to get list of files
    const query = await dv.query(`
      TABLE
        on,
        time,
        category,
        details,
        key
      FROM "Piano/Practice" 
      `)

    let categories = {
      "scales": 0,
      "chords": 0,
      "piece": 0,
      "improv": 0
    }
    
    const entries = query.value.values;
    let notes = []
  
    entries.forEach(entry => {
      if(this.isPracticeFileValid(entry)) {
        const noteData = 
        {
          filename: entry[0].path,
          on: Util.asArray(entry[1]).map(link => link.path),
          time: Util.asArray(entry[2]).map(this.convertPlayingTimeToInteger),
          category: entry[3],
          details: entry[4] ?? "",
          keys: entry[5] != null ? Util.asArray(entry[5]) : []
        }
  
        if (noteData.details.contains("improv")) {
          categories["improv"] += Util.sum(noteData.time)
        }
  
        categories[noteData.category] += Util.sum(noteData.time)
        
        notes.push(noteData)
      }
    })
  
    return [notes, categories]
  }

  isPracticeFileValid(entry) {
    // the file should at least contain one or more entries for [on:: ] and [time:: ]
    return entry[1] != null && entry[2] != null;
  }

  calcDailyPlayTime(dv) {
    const {Util} = customJS

    const note = dv.current()
    const currentPath = note.file.path

    if (note.played == null) return 0;

    let paths = Util.asArray(note.played)
    .filter(x => x && x.path) // remove null/undefined; the Played entries can be read as invalid as they're being typed out
    .map(e => e.path)

    const queryString = paths.join("\" or \"")
    const pages = dv.pages(`"${queryString}"`)

    const totals = pages.map(page => {
      const pairs = this.zipPagePathsAndPlayTimes(Util, page)
      return this.sumTimesForCurrentDate(pairs, currentPath)
    }).values
    
    return Util.sum(totals);
  }

  sumTimesForCurrentDate(pairs, currentPath) {
    return pairs
      .reduce(((acc, val) => 
        val[0].includes(currentPath) 
          ? acc + val[1] 
          : acc), 0);
  }

  zipPagePathsAndPlayTimes(Util, page) {
    return Util.zip(
      this.extractPagePaths(Util, page), 
      this.extractPageTimes(Util, page))
  }

  extractPageTimes(Util, page) {
    return Util.asArray(page.time).map(this.convertPlayingTimeToInteger);
  }

  extractPagePaths(Util, page) {
    return Util.asArray(page.on).map(x => x.path);
  }

  calcPracticeFilePlayTime(dv) {
    const note = dv.current();
    return this.sumPlayingTime(note);
  }

  sumPlayingTime(note) {
    const {Util} = customJS;
    if (note.time == null || note.time.length == 0) return 0;

    const times = Util.asArray(note.time)
                  .filter(x => x)
                  .map(this.convertPlayingTimeToInteger);

    return Util.sum(times);
  }

  convertPlayingTimeToInteger(duration) {
    // duration could be a Duration object, an integer, or possibly even a string
    // (e.g. it says "min" or "minutes" but the number hasn't been filled in yet, so it isn't parsed as a Duration)
    // we'll only return a value if it's a number or a Duration object.
    if (typeof(duration) === 'number') return duration;
    if (typeof(duration) === 'object' && 'values' in duration) return duration.values["minutes"];

    return 0;
  }

  // depreciated! 
  calcTotalPlayTime(note) {
    const {Util} = customJS
    
    if (note == null) return 0;
    // scenarios: 
    // - single integer
    // - single tuple (1d array)
    // - list of integers (1d array)
    // - list with tuples nd integers (compound array) 
    // - list of tuples (2d array)

    if (note != null) {
      let sum = 0
      
      if (Number.isInteger(note)) {
        sum = note
      }
    
      else if (Util.isIntArray(note)) {
        sum = note.reduce((acc,val) => acc + val, 0)
      }
    
      else if (Util.isSingleTuple(note)) {
        sum = note[note.length-1]
      }
    
      else if (Util.isCompoundList(note)) {
        note.forEach(elem => {
          if (Util.isValidTuple(elem)) sum += elem[elem.length-1]
          else if (Number.isInteger(elem)) sum += elem
        })
      }
    
      else if (Util.isListOfTuples(note)) {
        console.log("is tuple list\n" + note)
        note.forEach(elem => 
          sum = sum + elem[elem.length-1])
      }
    
      return sum;
    }
  }
}