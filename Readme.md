## General

This project defines utility functions for creating summary statistics in Obsidian.
It is meant to work alongside these Obsidian plug-ins:
- [Dataview](https://blacksmithgu.github.io/obsidian-dataview/)
- [CustomJS](https://github.com/saml-dev/obsidian-custom-js)


## Example Usage

The dataview.js code assumes there is a Piano/Practice directory in Obsidian that contains markdown documents for each piece or exercise worked on. Each file will have a sequence of entries for the date practiced and the total time spent that day.

This code:

```dataviewjs
const {Util, Piano} = customJS

const [noteData, categories] = await Piano.extractNoteData(dv)
const count = await Piano.getDailyEntryCount(dv)
const sumMinutes = Util.sum(noteData.map(note => Util.sum(note.time)))
const avg = Math.round(sumMinutes / count, 2)

dv.paragraph(`**${count} total days logged**`)
dv.paragraph(`**${Util.ToHoursAndMinutes(sumMinutes)} total time logged**`)
dv.paragraph(`**${avg} minutes per day on average**`)
dv.paragraph("---")
dv.paragraph(`**${Util.ToHoursAndMinutes(categories["scales"])} spent on scales**`)
dv.paragraph(`**${Util.ToHoursAndMinutes(categories["chords"])} spent on chords**`)  
dv.paragraph(`**${Util.ToHoursAndMinutes(categories["improv"])} spent on improvisation**`)
dv.paragraph(`**${Util.ToHoursAndMinutes(categories["piece"])} spent on repertoire**`)

const sumSightReading = Piano.getSightReadingTime(dv)
dv.paragraph(`**${Util.ToHoursAndMinutes(sumSightReading)} spent on sight-reading**`)

```

Generates:

![A simple breakdown of time spent on piano practice](example.png)